#!env/bin/python
# -*- coding: utf-8 -*-
"""
    Simulator - a simulator plugin for homeservio homeserver
    
    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
import time
from datetime import datetime
import random
import logging

from homeservio.plugin import Plugin
from homeservio.plugin.api import register_device_change_request, send_device_update_event, DeviceFactory, Api

log = logging.getLogger()
class Simulator(Plugin):
    """
    The simulator creates a couple of devices that it reports. It acts as any homeserver
    simulating interaction with live.
    
    * The inverter flips on and off every minute
    * The temperature and humidity are random values
    * Testsw1 flips every minute.
    * Testdimmer1 flips also every minute.
    * Turning on the aircondition reduces temperature immediately with 1 degree.
    * Turning on the heater increases temperature immediately with 1 degree.
    * All other devices are not automatically controlled. 
    """
    
    def init(self, name, settings):
        register_device_change_request(self.device_change_request)
        self.devices={}
        self.devices["inverter"] = DeviceFactory.inverter("inverter", state = "on", mode = "auto", temp = 23, fan = "auto")
        self.devices["11"] = DeviceFactory.multi_gauge("11", [
                                                       ("temperature", u"°C", 19),
                                                       ("humidity", u"% RH", 70),
                                                       ])
        self.devices["testsw1"] = DeviceFactory.switch("testsw1", "on")
        self.devices["testdimmer1"] = DeviceFactory.dimmer("testdimmer1", "off", 50)
        self.devices["2"] = DeviceFactory.switch("testsw2", "on")
        self.devices["d2"] = DeviceFactory.dimmer("testdimmer2", "off", 50)
        self.devices["3"] = DeviceFactory.switch("testsw3", "on")
        self.devices["d3"] = DeviceFactory.dimmer("testdimmer3", "off", 43)
        self.devices["d4"] = DeviceFactory.dimmer("testdimmer4", "off", 44)
        self.devices["ac"] = DeviceFactory.switch("aircondition", "on")
        self.devices["heater2"] = DeviceFactory.switch("heater", "off")

        
    def report_all(self):
        log.debug("report all")
        for address, device in self.devices.iteritems():
            log.debug("Update %s %s", str(address), str(device))
            send_device_update_event("simulator." + address, device.toJson(meta=True))
             
    def run(self, name, settings):
        log.error("starts")
        self.report_all()
        Api.instance().start()
        
        while 1:
            inverter = self.devices["inverter"]
            inverter.last_change = datetime.utcnow()
            if inverter.state == "on":
                inverter.state = "off"
            else:
                inverter.state = "on"
            send_device_update_event("simulator.inverter", inverter.toJson())
            log.debug(inverter)
            time.sleep(1)
            
            sensor = self.devices["11"]
            sensor.last_change = datetime.utcnow()
            sensor.temperature = round(sensor.temperature + float(random.randint(-2,2))/10, 1)
            send_device_update_event("simulator.11", sensor.toJson())
            log.debug(sensor)
            time.sleep(1)
            
            sensor.humidity = round(sensor.humidity + (random.randint(-1,1)), 1)
            send_device_update_event("simulator.11", sensor.toJson())
            log.debug(sensor)
            time.sleep(1)
            
            sw1 =  self.devices["testsw1"]
            if sw1.state == "on":
                sw1.state = "off"
            else:
                sw1.state = "on"
            log.debug(sw1)
            send_device_update_event("simulator.testsw1", sw1.toJson())
            
            dim1 = self.devices["testdimmer1"]
            if dim1.state == "on":
                dim1.state = "off"
            elif dim1.state == "off":
                dim1.state = "dim"
            else:
                dim1.state = "on"
            log.debug(dim1)
            send_device_update_event("simulator.testdimmer1", dim1.toJson())

            time.sleep(60)
            
    def device_change_request(self, devname, device):
        log.debug("dcr %s %s", devname, device)
        shortname = devname[len("simulator."):]
        if self.devices.has_key(shortname):
            dev = self.devices.get(shortname)
            diff = dev.updateFromString(device[1])
            if diff is not None:
                log.info("Changing device %s %s", devname, diff)
                # note we really do not do anything here, just update our own db
                send_device_update_event(devname, dev.toJson())
                if shortname == "heater2":
                    try:
                        if diff.state == "on":
                            self.devices["11"].temperature = self.devices["11"].temperature + 1
                        else:
                            self.devices["11"].temperature = self.devices["11"].temperature - 1
                    except AttributeError:
                        pass
                if shortname == "ac":
                    try:
                        if diff.state == "on":
                            self.devices["11"].temperature = self.devices["11"].temperature - 2
                            self.devices["11"].humidity = self.devices["11"].humidity - 1
                        else:
                            self.devices["11"].temperature = self.devices["11"].temperature + 1
                            self.devices["11"].humidity = self.devices["11"].humidity + 1
                    except AttributeError:
                        pass
        else:
            log.debug("Not my device: %s", devname)
            
