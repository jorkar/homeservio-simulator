# Homeservio simulator

A simulator for homeservio homeserver.

For more info see: <https://homeservio.com/plugins/homeservio-simulator>

The simulator creates a couple of devices that it reports. It acts as any homeserver
simulating interaction with live.

*   The cpu and memory are actual figures.
*   The inverter flips on and off every minute
*   The temperature and humidity are random values
*   Testsw1 flips every minute.
*   Testdimmer1 flips also every minute.
*   Turning on the aircondition reduces temperature immediately with 1 degree.
*   Turning on the heater increases temperature immediately with 1 degree.
*   All other devices are not automatically controlled.

## Installation

The simulator can be installed via

'homeserver plugin install simulator'

## Configuration

The simulator is configured in the [plugin:simulator] section. 

disabled: If disabled is set, the plugin do not start automatically at upstart
          of homeserver.

## License information

    Copyright (C) 2014 Jörgen Karlsson. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Supported platforms

Currently tested on Linux/debian with Python 2.7.

##Release notes
### 0.1.7
- timing error
### 0.1.6
- name fault fixed

### 0.1.3
- rounding of figures
- remove cpu and memory gauge (since it's now built-in in homeserver)

### 0.1.2
- fix for device change request that did not work

### 0.1.0
- Initial release