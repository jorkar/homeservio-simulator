# -*- coding: utf-8 -*-
"""
    Simulator - a simulator plugin for homeservio homeserver
    
    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""
from setuptools import setup, find_packages
from os import getcwd

__version__ = "unknown"
exec(open('homeservio_plugins/simulator_version.py').read())

setup(name="homeservio_simulator",
      version=__version__,
      author= 'Jörgen Karlsson',
      author_email='jorgen@karlsson.com',
      description='Simulator - a simulator plugin for homeservio homeserver',
      long_description=open('README.txt').read(),
      packages=['homeservio_plugins'],
      url = "https://homeservio.com",
      install_requires = [
        "psutil>=2.1.1",
        ],
      dependency_links=['http://homeservio.com:8080/simple'],
      classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Home Automation",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7",
        "Unreleased :: Do not upload to PYPI"
        ]
      )
